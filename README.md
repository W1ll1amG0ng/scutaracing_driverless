# SCUTAracing Driverless
华南理工大学无人驾驶方程式赛车队无人系统代码库    

**您位于 `main` 分支**    
**`main`分支仅作为模板，不要将针对于特定赛季的代码提交至此分支！**   
**切换分支时，`git checkout`需要加上选项`--recurse-submodules`！！！**   
**否则会导致子模块签出不正确**

# 依赖
平台：ROS Noetic

# 构建 & 部署
1. clone 仓库
1. 迁出到需要使用的分支：`git checkout <branch-name>`
1. 拉取所有子模块：`git submodule update --init --recursive`
1. 初始化工作空间：`catkin_init_workspace`
1. 使用 `catkin-tools` 进行编译：`catkin build`

# 代码管理说明
分支以赛季为分类，如23赛季的代码位于`2023`分支。     
除了部分依赖库（如`fsd_common_msgs`）外，其余包均以 git submodule 的形式并入代码库

## 如何将你的项目作为子模块添加进代码库
1. 将你的项目作为一个单独的仓库上传
1. 在 gitlab 上将该代码库 fork 到你的账号下
1. clone 代码库，选择你要提交的代码库分支
1. 在该代码库目录下：`git submodule add <your-repo-url> src/<your-repo-name>`
1. 提交
1. 确认提交无误后发布 pull request